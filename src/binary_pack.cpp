#include "binary_pack.h"

// Reader

BinaryReader::BinaryReader ( std::istream& in_ ) : in( in_ ) {}

void BinaryReader::readInt ( uint32_t& val ) {
    in.read(reinterpret_cast<char*>(&val), sizeof(val));
}

void BinaryReader::readFloat ( float& val ) {
    in.read(reinterpret_cast<char*>(&val), sizeof(val));
}

void BinaryReader::readBool ( bool& val ) {
    in.read(reinterpret_cast<char*>(&val), sizeof(val));
}

// Writer

BinaryWriter::BinaryWriter ( std::ostream& out_ ) : out( out_ ) {}

void BinaryWriter::writeInt ( const uint32_t val ) {
    out.write(const_cast<char*>(reinterpret_cast<const char*>(&val)), sizeof(val));
}

void BinaryWriter::writeFloat ( const float val ) {
    out.write(const_cast<char*>(reinterpret_cast<const char*>(&val)), sizeof(val));
}

void BinaryWriter::writeBool ( const bool val ) {
    out.write(const_cast<char*>(reinterpret_cast<const char*>(&val)), sizeof(val));
}
