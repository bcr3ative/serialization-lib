#include <iostream>
#include <fstream>

#include "text_pack.h"
#include "binary_pack.h"

class Object : public Serializable {
public:
    Object(int count_, bool alive_, float num_) : count(count_), alive(alive_), num(num_) {}
    Object() {}

    void print() {
        std::cout << "count:" << count << " alive:" << alive << " float:" << num << std::endl; 
    }

    void write(Writer& w) override {
        w.writeInt(count);
        w.writeBool(alive);
        w.writeFloat(num);
    }

    void read(Reader& r) override {
        r.readInt(count);
        r.readBool(alive);
        r.readFloat(num);
    }
private:
    uint32_t count;
    bool alive;
    float num;
};

int main() {

    Object obj(2, false, 0.1437276201);
    Object obj2(15, true, 2.3456789);
    Object obj3;

    {
        std::ofstream out("object.txt");
        TextWriter w(out);
        obj.write(w);
    }

    {
        std::ifstream in("object.txt");
        TextReader r(in);
        obj3.read(r);
    }

    obj3.print();

    {
        std::ofstream out("object.dat", std::ios::binary);
        BinaryWriter w(out);
        obj2.write(w);
    }

    {
        std::ifstream in("object.dat", std::ios::binary);
        BinaryReader r(in);
        obj3.read(r);
    }

    obj3.print();

    return 0;
}
