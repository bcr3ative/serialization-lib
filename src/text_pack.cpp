#include "text_pack.h"

// Reader

TextReader::TextReader ( std::istream& in_ ) : in( in_ ) {}

void TextReader::readInt ( uint32_t& val ) {
    in >> val;
}

void TextReader::readFloat ( float& val ) {
    in >> val;
}

void TextReader::readBool ( bool& val ) {
    in >> val;
}

// Writer

TextWriter::TextWriter ( std::ostream& out_ ) : out( out_ ) {}

void TextWriter::writeInt ( const uint32_t val ) {
    out << " " << val;
}

void TextWriter::writeFloat ( const float val ) {
    out << " " << val;
}

void TextWriter::writeBool ( const bool val ) {
    out << " " << val;
}
