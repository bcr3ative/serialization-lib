#ifndef SLIB_TEXTPACK_H_
#define SLIB_TEXTPACK_H_

#include <fstream>

#include "core.h"

class TextReader : public Reader {
public:
    TextReader(std::istream& in_);

    void readInt(uint32_t& val) override;
    void readFloat(float&  val) override;
    void readBool(bool&  val) override;
private:
    std::istream& in;
};

class TextWriter : public Writer {
public:
    TextWriter(std::ostream& out_);

    void writeInt(const uint32_t  val) override;
    void writeFloat(const float  val) override;
    void writeBool(const bool  val) override;
private:
    std::ostream& out;
};

#endif