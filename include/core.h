#ifndef SLIB_CORE_H_
#define SLIB_CORE_H_

#include <cstdint>

class Reader {
public:
    virtual void readInt(uint32_t& val) = 0;
    virtual void readFloat(float& val) = 0;
    virtual void readBool(bool& val) = 0;
};

class Writer {
public:
    virtual void writeInt(const uint32_t val) = 0;
    virtual void writeFloat(const float val) = 0;
    virtual void writeBool(const bool val) = 0;
};

class Serializable {
public:
    virtual void read(Reader& r) = 0;
    virtual void write(Writer& w) = 0;
};

#endif